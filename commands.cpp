#include "commands.h"

void cierra(int argc, const char *argv[])
{
	RF_Engine::Status() = false;
}

void taskList(int argc, const char *argv[])
{
	for(RF_Process *p : RF_TaskManager::instance->getTaskList())
	{
		if(p->type != "RF_Text")
		{
			RF_DebugConsoleListener::writeLine(p->id + " -> " + p->type);
		}
	}
}

void getRoomSize(int argc, const char *argv[])
{
  RF_DebugConsoleListener::writeLine("Room size: " + to_string((int)Camera::RoomSize().x) + "x" + to_string((int)Camera::RoomSize().y));
}

void setRoomSize(int argc, const char *argv[])
{
  Camera::RoomSize(atoi(argv[0]), atoi(argv[1]));
  RF_DebugConsoleListener::writeLine("Room size setted");
}

void LoadCommands()
{
  RF_DebugConsoleListener::addCommand("close", new RF_DebugCommand("cierra el juego", 0, &cierra));
  RF_DebugConsoleListener::addCommand("taskList", new RF_DebugCommand("lista los procesos", 0, &taskList));
  RF_DebugConsoleListener::addCommand("getRoomSize", new RF_DebugCommand("devuelve el tamaño de la habitación", 0, &getRoomSize));
  RF_DebugConsoleListener::addCommand("setRoomSize", new RF_DebugCommand("establece el tamaño de la habitación", 2, &setRoomSize));
}

#include <RosquilleraReforged/rf_engine.h>

#include <unistd.h>
#include <iostream>
#include <string>
using namespace std;

#include "include/mainprocess.h"
#include "commands.h"

extern string startingScene;

int main(int argc, char *argv[])
{
	if(argc > 0)
	{
		string arg(argv[0]);
	  arg.erase(arg.find_last_of('/'), arg.size());
		chdir(arg.c_str());

		for(int i = 1; i < argc; i++)
		{
			if(strcmp(argv[i], "--scene") == 0 || strcmp(argv[i], "-s") == 0)
			{
				startingScene = argv[++i];
			}
		}
	}

	LoadCommands();
	RF_Engine::Start<MainProcess>(true);
	return 0;
}

#ifdef WIN
	int __stdcall WinMain(void*, void*, char*, int)
	{
		main(0, NULL);
		return 0;
	}
#endif

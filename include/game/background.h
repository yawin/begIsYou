#ifndef BACKGROUND_H
#define BACKGROUND_H

#include <RosquilleraReforged/rf_engine.h>
#include <RosquilleraReforged/rf_process.h>
using namespace RF_Structs;

#include <BatterEngine/actor.h>

class Background : public RF_Process
{
  public:
    Background():RF_Process("Background"){}
    virtual ~Background(){}

    void Start();
};

#endif //BACKGROUND_H

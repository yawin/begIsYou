#ifndef GAMEOBJECT_H
#define GAMEOBJECT_H

#include <RosquilleraReforged/rf_process.h>
using namespace RF_Structs;

#include <BatterEngine/actor.h>

class Gameobject : public Actor
{
  public:
    Gameobject():Actor("Gameobject"){}
    virtual ~Gameobject(){}

    void Configure(string _type, int x, int y);
    bool DoMove(int x, int y);

  private:
    string goType;

    void OnUpdate();

    float cooldown = 0.0;
    bool moved = false;
};

#endif //GAMEOBJECT_H

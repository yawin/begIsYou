#ifndef SPLASH_H
#define SPLASH_H

#include <BatterEngine/scene.h>
#include <RosquilleraReforged/rf_layer.h>

class Splash : public Scene
{
  public:
    Splash():Scene("SplashScreen"){}
    virtual ~Splash(){}

    void SceneUpdate();
    void Configure();

  private:
    class SplashScreen : public RF_Layer
    {
        public:
            SplashScreen():RF_Layer("SplashScreen"){}
            virtual ~SplashScreen(){}

            virtual void Start();
            virtual void Update();
            virtual void Dispose();

        private:
            vector<SDL_Surface*> bgImg;
            float step = 0.0, stp;
            int i,j, xx, yy;
            int logo = 0, max_logos;
    };
};

#endif //SPLASH_H

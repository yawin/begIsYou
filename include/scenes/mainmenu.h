#ifndef MAINMENU_H
#define MAINMENU_H
#include <BatterEngine/scene.h>

#include "cursor.h"
#include "label.h"

class MainmenuCursor : public Cursor
{
  public:
    MainmenuCursor():Cursor(){}
    virtual ~MainmenuCursor(){}

    void Selection(unsigned int opc);
};

class Mainmenu : public Scene
{
  public:
    Mainmenu():Scene("Mainmenu"){}
    virtual ~Mainmenu(){}

    enum {
      _START,
      _OPTIONS,
      _EXIT,
      _FOO_OPT
    };

    LabelFormat optionList[_FOO_OPT];

    virtual void SceneUpdate();
    virtual void Configure();

  private:
    //BackgroundImage *bgImg;
    Label *l;
    //int i;
    MainmenuCursor *c;
};

#endif //MAINMENU_H

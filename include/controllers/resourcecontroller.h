#ifndef RESOURCECONTROLLER_H
#define RESOURCECONTROLLER_H

#include <RosquilleraReforged/rf_engine.h>
#include <RosquilleraReforged/rf_process.h>
#include <RosquilleraReforged/utils/parser.h>
using namespace RF_Structs;

#include <unordered_map>
using namespace std;

#include <BatterEngine/actor.h>

namespace ResourceController
{
  class Resource
  {
    public:
      Resource(){}
      ~Resource(){}

      string sourcename;
  };

  extern unordered_map<string, Resource> Resources;

  extern void LoadResources();
};

#endif //RESOURCECONTROLLER_H

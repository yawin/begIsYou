#ifndef GAMECONTROLLER_H
#define GAMECONTROLLER_H

#include <RosquilleraReforged/rf_process.h>
#include <RosquilleraReforged/utils/parser.h>
using namespace RF_Structs;

#include <unordered_map>
using namespace std;

#include <BatterEngine/actor.h>

class GameController : public RF_Process
{
  public:
    static GameController* instance;

    GameController():RF_Process("GameController"){}
    virtual ~GameController(){}

    void Start();
    void Update();

    void LoadLevel(string level, string sceneid);
    void AddElement(string element);

    bool isThat(string element, string attribute);
    void setAttribute(string element, string attribute, bool value);

    Uint32 RoomColor;
    Vector2<int> RoomSize;

    Vector2<int> you;
    float youCoolDown;

  private:
    unordered_map<string, unordered_map<string, bool>> elements;
};

#endif //GAMECONTROLLER_H

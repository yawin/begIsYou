#ifndef CONFIGURATIONCONTROLLER_H
#define CONFIGURATIONCONTROLLER_H

#include <RosquilleraReforged/rf_engine.h>
#include <RosquilleraReforged/utils/parser.h>

#include <map>
#include <vector>
using namespace std;
using namespace RF_Structs;

class ConfigurationController
{
  public:
    static ConfigurationController *instance;
    virtual ~ConfigurationController();

    static string getConfig(string field);
    static void setConfig(string field, string value);
    static RF_Window* MainWindow() {return RF_Engine::MainWindow();}

    static vector<string> saveList();
    static void setSaveFile(string file);
    static void saveGame();
    static void loadGame();

    static string getGameData(string key);
    static void setGameData(string key, string val);
    map<string, string> gameData;

    bool escPressed = false;
    static bool& EscPressed();

    static const string resFolder;
    static const string dataFolder;
    static const string localeFolder;
    static const string saveFolder;
    static const string levelFolder;
    static const string gfxFolder;

  private:
    static void checkInstance();

    ConfigurationController();

    Parser *parser;
    string saveFile = "";
};

#endif //CONFIGURATIONCONTROLLER_H

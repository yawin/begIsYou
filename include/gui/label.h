#ifndef LABEL_H
#define LABEL_H

#include <RosquilleraReforged/rf_structs.h>
using namespace RF_Structs;

#include <HoneyElements/label.h>
#include <string>
using namespace std;

typedef struct
{
  int opt;
  string key;
  Vector2<float> position;
  Vector2<float> sombra;
  Vector2<float> cursorOffset;
} LabelFormat;

class Label : public HoneyLabel
{
	protected:
		virtual void textUpdate();

	public:
		string key;
};

#endif //LABEL_H

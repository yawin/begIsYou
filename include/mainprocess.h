#ifndef MAINPROCESS_H
#define MAINPROCESS_H

#include <RosquilleraReforged/rf_engine.h>
#include <RosquilleraReforged/rf_process.h>
using namespace RF_Structs;

class MainProcess : public RF_Process
{
	public:
		MainProcess():RF_Process("MainProcess"){}

		void Start();
		void Update();

		static void NavigateTo(string scene, bool top = true);
		static void NavigateBack();

	private:
		bool fscrn;

		static MainProcess* instance;

};

#endif //MAINPROCESS_H

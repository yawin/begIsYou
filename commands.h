#ifndef COMMANDS_H
#define COMMANDS_H

#include <RosquilleraReforged/rf_engine.h>
#include <RosquilleraReforged/rf_debugconsole.h>

#include <BatterEngine/camera.h>

extern void cierra(int argc, const char *argv[]);
extern void taskList(int argc, const char *argv[]);
extern void getRoomSize(int argc, const char *argv[]);
extern void setRoomSize(int argc, const char *argv[]);

extern void LoadCommands();

#endif //COMMANDS_H

#include "label.h"
#include <BatterEngine/utils/locale.h>

#include <RosquilleraReforged/rf_engine.h>

void Label::textUpdate()
{
  text_to_write = Locale::getText(key);
}

#include "mainmenu.h"

#include <RosquilleraReforged/rf_engine.h>
#include <RosquilleraReforged/rf_assetmanager.h>
#include <RosquilleraReforged/rf_textmanager.h>
#include <RosquilleraReforged/rf_soundmanager.h>
#include <BatterEngine/inputmanager.h>
#include <BatterEngine/scenemanager.h>

#include "configurationcontroller.h"
#include "mainprocess.h"

void MainmenuCursor::Selection(unsigned int opc)
{
  switch(opc)
  {
    case 0:
      /*if(ConfigurationController::getConfig("savefile") == ""){ConfigurationController::setConfig("savefile", to_string(rand()));}
      Configuration::setSaveFile(ConfigurationController::getConfig("savefile"));
      Configuration::loadGame();

      if(ConfigurationController::getGameData("startingScene") == ""){ConfigurationController::setGameData("startingScene", ConfigurationController::getConfig("startingScene"));}
      ConfigurationController::instance->playingMap = ConfigurationController::getGameData("startingScene");
      SceneManager::Start(Configuration::getGameData("startingScene"));*/
      RF_Engine::Debug("Empezar partida");
      break;
    case 1:
      //SceneManager::Start("OptionMenu");
      RF_Engine::Debug("Menú de opciones");
      break;
    case 2:
      MainProcess::NavigateTo("SplashScreen", false);
      //RF_Engine::Status() = false;
      break;
  }
}

void Mainmenu::SceneUpdate()
{
  //RF_SoundManager::changeMusic("gui", "mainmenu");

  if(InputManager::getAxis("exeControl") != 0)
  {
    if(!ConfigurationController::EscPressed())
    {
      RF_Engine::Status() = false;
      ConfigurationController::EscPressed() = true;
    }
  }
  else
  {
    ConfigurationController::EscPressed() = false;
  }
}

void Mainmenu::Configure()
{
  RF_AssetManager::loadAssetPackage(ConfigurationController::resFolder + "gui");

  optionList[_START] = {_START, "mainmenu_start", {200, 275}, {2.0, 2.0}, {50.0,10.0}};
  optionList[_OPTIONS] = {_OPTIONS, "mainmenu_options", {200, 325}, {2.0, 2.0}, {50.0,10.0}};
  optionList[_EXIT] = {_EXIT, "mainmenu_exit", {200, 375}, {2.0, 2.0}, {50.0,10.0}};

  /*bgImg = AddElement<BackgroundImage>();
  bgImg->setImg("gui", "mainmenu_background");
  bgImg->transform.position = {(float)(Configuration::MainWindow()->width() >> 1), (float)(Configuration::MainWindow()->height() >> 1)};*/

  for(int i = 0; i < _FOO_OPT; i++)
  {
    l = RF_Engine::getTask<Label>(RF_Engine::newTask<Label>(id));
    l->key = optionList[i].key;
    l->transform.position = optionList[i]. position;
    //scale
    l->setHorizontalAlign(HoneyLabel::_ALIGN_RIGHT);
    l->setShadowPosition(optionList[i].sombra);
    l->setTextColor({255,255,255});
    l->setFont(RF_AssetManager::Get<RF_Font>("gui", "Debby"));
  }

  c = AddElement<MainmenuCursor>();
  c->setOptionList(optionList, _FOO_OPT);
}

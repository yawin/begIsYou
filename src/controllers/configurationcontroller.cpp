#include "configurationcontroller.h"
#include <BatterEngine/inputmanager.h>

#include <sys/stat.h>
#include <dirent.h>
#include <string>
#include <iostream>
#include <fstream>
using namespace std;

const string ConfigurationController::resFolder = "resources/";
const string ConfigurationController::localeFolder = "locale/";
const string ConfigurationController::dataFolder = ConfigurationController::resFolder + "data/";
const string ConfigurationController::saveFolder = ConfigurationController::dataFolder + "saves/";
const string ConfigurationController::levelFolder = ConfigurationController::dataFolder + "levels/";
const string ConfigurationController::gfxFolder = ConfigurationController::resFolder + "gfx/";

ConfigurationController* ConfigurationController::instance = nullptr;
void ConfigurationController::checkInstance()
{
  if(instance == nullptr)
  {
    new ConfigurationController();
  }
}

ConfigurationController::ConfigurationController()
{
  instance = this;
  parser = new Parser(dataFolder + "options.json");
}

ConfigurationController::~ConfigurationController()
{
  instance = nullptr;
}

string ConfigurationController::getConfig(string field)
{
  checkInstance();

  string val;
  Parser::json_string(instance->parser->jsvalue["config"][field], val);
  return val;
}

void ConfigurationController::setConfig(string field, string value)
{
  checkInstance();

  instance->parser->jsvalue["config"][field] = value;
  ofstream salida(dataFolder + "options.json");
  Json::StyledStreamWriter writer;
  writer.write(salida, instance->parser->jsvalue);
}

vector<string> ConfigurationController::saveList()
{
  vector<string> files;
  DIR *dir = opendir(ConfigurationController::saveFolder.c_str());
  if(dir == nullptr)
  {
    RF_Engine::Debug("Save system [Error]: No se puede abrir el directorio");
    return files;
  }

  struct dirent *ent;
  string name;

  while((ent = readdir(dir)) != nullptr)
  {
    if((strcmp(ent->d_name, ".")!=0) && (strcmp(ent->d_name, "..")!=0))
    {
      name = ent->d_name;
      const size_t pos = name.find_last_of('.');
      name.erase(pos);
      files.push_back(name);
    }
  }

  return files;
}

void ConfigurationController::setSaveFile(string file)
{
  checkInstance();
  instance->saveFile = file;
  RF_Engine::Debug("Asignado fichero de guardado: " + file);
}

void ConfigurationController::saveGame()
{
  checkInstance();

  struct stat buffer;
  if(stat((ConfigurationController::saveFolder + instance->saveFile + ".json").c_str(), &buffer) != 0)
  {
    ofstream s(ConfigurationController::saveFolder + instance->saveFile + ".json");
    s << "{}";
    s.close();
  }

  Parser *saveParser = new Parser(ConfigurationController::saveFolder + instance->saveFile + ".json");
  for(auto m = instance->gameData.begin(); m != instance->gameData.end(); m++)
  {
    saveParser->jsvalue["gameData"][m->first] = m->second;
  }

  ofstream salida(ConfigurationController::saveFolder + instance->saveFile + ".json");
  Json::StyledStreamWriter writer;
  writer.write(salida, saveParser->jsvalue);

  delete saveParser;
}

void ConfigurationController::loadGame()
{
  checkInstance();

  struct stat buffer;
  if(stat((ConfigurationController::saveFolder + instance->saveFile + ".json").c_str(), &buffer) != 0)
  {
    RF_Engine::Debug("Load system [Error]: El archivo de guardado indicado no existe");
    return;
  }

  Parser *loadParser = new Parser(ConfigurationController::saveFolder + instance->saveFile + ".json");
  instance->gameData.clear();
  for(auto m : loadParser->jsvalue["gameData"].getMemberNames())
  {
    Parser::json_string(loadParser->jsvalue["gameData"][m], instance->gameData[m]);
  }

  delete loadParser;
}

string ConfigurationController::getGameData(string key)
{
  checkInstance();
  return (instance->gameData.find(key) != instance->gameData.end()) ? instance->gameData[key] : "";
}

void ConfigurationController::setGameData(string key, string val)
{
  checkInstance();
  instance->gameData[key] = val;
}

bool& ConfigurationController::EscPressed()
{
  ConfigurationController::checkInstance();
  return ConfigurationController::instance->escPressed;
}

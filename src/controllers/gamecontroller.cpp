#include "gamecontroller.h"

#include <RosquilleraReforged/rf_engine.h>
#include <BatterEngine/camera.h>
#include <BatterEngine/inputmanager.h>

#include "configurationcontroller.h"
#include "background.h"
#include "gameobject.h"

GameController* GameController::instance = nullptr;

void GameController::Start()
{
  if(instance != nullptr)
  {
    signal = S_KILL;
    return;
  }

  instance = this;
  RF_Engine::newTask<Camera>(id);
}

void GameController::LoadLevel(string level, string sceneid)
{
  //Cargamos el json del nivel
    Parser *parser = new Parser(ConfigurationController::levelFolder + level + ".json");

  //Obtenemos el tamaño de la habitación
    Parser::json_vector2int(parser->jsvalue["RoomSize"], RoomSize);
    Camera::RoomSize(RoomSize.x, RoomSize.y);

  //Obtenemos el color de la habitación
    RoomColor = parser->jsvalue["RoomColor"].asUInt();
    RF_Engine::newTask<Background>(sceneid);

    string type;
    int x, y;
    Gameobject* go;
    for(Json::Value jv : parser->jsvalue["map"])
    {
      Parser::json_string(jv["type"], type);
      Parser::json_int(jv["x"], x);
      Parser::json_int(jv["y"], y);

      go = RF_Engine::getTask<Gameobject>(RF_Engine::newTask<Gameobject>(sceneid));
      go->Configure(type, x, y);
    }

    delete parser;
}

void GameController::Update()
{
  float axis_x = InputManager::getAxis("Horizontal");
  float axis_y = InputManager::getAxis("Vertical");

  you.x = 0;
  you.y = 0;

  if(youCoolDown > 0.0)
  {
    youCoolDown -= RF_Engine::instance->Clock.deltaTime;
  }
  else if(
    (axis_x != 0.0 && axis_y == 0.0) ||
    (axis_x == 0.0 && axis_y != 0.0)
  )
  {
    youCoolDown = 0.15;

    you.x = axis_x;
    you.y = axis_y;
  }

  if(axis_x == 0.0 && axis_y == 0.0)
  {
    youCoolDown = 0.0;
  }
}

void GameController::AddElement(string element)
{
  unordered_map<string, unordered_map<string, bool>>::const_iterator e = elements.find(element);
  if(e == elements.end())
  {
    elements[element];
  }
}

bool GameController::isThat(string element, string attribute)
{
    unordered_map<string, bool>::const_iterator e = elements[element].find(attribute);
    if(e == elements[element].end())
    {
      elements[element][attribute] = false;
    }

    return elements[element][attribute];
}

void GameController::setAttribute(string element, string attribute, bool value)
{
  elements[element][attribute] = value;
}

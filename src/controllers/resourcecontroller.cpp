#include "resourcecontroller.h"

#include <RosquilleraReforged/rf_engine.h>
#include "configurationcontroller.h"

unordered_map<string, ResourceController::Resource> ResourceController::Resources;

void ResourceController::LoadResources()
{
  Parser *parser = new Parser(ConfigurationController::dataFolder + "gameobjects.json");

  string id, package;
  for(Json::Value jv : parser->jsvalue["resources"])
  {
    Parser::json_string(jv["id"], id);
    Parser::json_string(jv["package"], package);

    ResourceController::Resource res;
    res.sourcename = package;

    ResourceController::Resources[id] = res;
  }
}

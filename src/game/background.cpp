#include "background.h"

#include <RosquilleraReforged/rf_engine.h>
#include <RosquilleraReforged/rf_primitive.h>
#include <BatterEngine/camera.h>

#include "gamecontroller.h"

void Background::Start()
{
  int tileSize = Camera::DrawModifier().scale.x;
  int tileH = Camera::RoomSize().x;
  int tileV = Camera::RoomSize().y;

  transform.position.x = RF_Engine::MainWindow()->width() >> 1;
  transform.position.y = RF_Engine::MainWindow()->height() >> 1;

  graph = SDL_CreateRGBSurface(0, tileSize*tileH, tileSize*tileV, 32, 0, 0, 0, 0);
  RF_Primitive::clearSurface(graph, GameController::instance->RoomColor);
  zLayer = -1;
}

#include "gameobject.h"

#include <RosquilleraReforged/rf_assetmanager.h>
#include <RosquilleraReforged/rf_taskmanager.h>
#include <BatterEngine/camera.h>

#include "configurationcontroller.h"
#include "gamecontroller.h"
#include "resourcecontroller.h"

void Gameobject::Configure(string _type, int x, int y)
{
    goType = _type;
    realPosition.x = x;
    realPosition.y = y;

    string sourcename = ResourceController::Resources[goType].sourcename;

    RF_AssetManager::loadAssetPackage(ConfigurationController::gfxFolder + "gameobjects/" + sourcename);
    animator->Add("idle", sourcename.c_str(), sourcename.c_str(), 3, 8.0, true);

    animationRelation["ST_STAND"] = "idle";
    addState("ST_STAND", [this]{this->OnUpdate();});

    GameController::instance->AddElement(goType);

    if(goType == "baba") GameController::instance->setAttribute("baba", "push", true);
    if(goType == "rock") GameController::instance->setAttribute("rock", "you", true);
    if(goType == "wall") GameController::instance->setAttribute("wall", "stop", true);
}

void Gameobject::OnUpdate()
{
  if(GameController::instance->isThat(goType, "you"))
  {
    if(GameController::instance->you.x < 0) flipType = FLIP_H;
    else if(GameController::instance->you.x > 0) flipType = FLIP_NONE;

    DoMove(GameController::instance->you.x, GameController::instance->you.y);
  }

  moved = false;
  //RF_Engine::Debug(to_string(Camera::DrawModifier().scale.x) + ", " + to_string(transform.scale.x));
}

bool Gameobject::DoMove(int x, int y)
{
  if(x == 0 && y == 0) return false;
  if(moved) return true;

  vector<Gameobject*> updateLate;

  vector<RF_Process*> e = RF_TaskManager::instance->getTaskByType(type);

  //Buscamos GameObjects que estén en las coordenadas a las que vamos
    for(RF_Process* p : e)
    {
      //Obtenemos el GameObject
        Gameobject* go = RF_Engine::getTask<Gameobject>(p->id);

      //Si el GameObject está en las coordenadas a las que vamos
        if(go->realPosition.x == realPosition.x + x &&
           go->realPosition.y == realPosition.y + y)
        {
          //Si es stop, dejamos de mirar y no nos movemos
            if(GameController::instance->isThat(go->goType, "stop"))
            {
              return false;
            }

          //Si no, lo añadimos a "procesar más tarde"
            if(GameController::instance->isThat(go->goType, "push"))
            {
              updateLate.push_back(go);
            }
        }
    }

    bool mov = true;
    for(Gameobject* g : updateLate)
    {
      mov = mov & g->DoMove(x,y);
    }

    float old_x = realPosition.x;
    float old_y = realPosition.y;

    if(mov) Move(x, y);

    return (old_x != realPosition.x || old_y != realPosition.y);
}

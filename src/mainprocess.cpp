#include "mainprocess.h"
#include <BatterEngine/utils/locale.h>
#include <BatterEngine/inputmanager.h>
#include <BatterEngine/scenemanager.h>

#include "configurationcontroller.h"
#include "gamecontroller.h"
#include "resourcecontroller.h"

#include "splash.h"
#include "mainmenu.h"

string startingScene = "SplashScreen";

MainProcess* MainProcess::instance = nullptr;

void MainProcess::Start()
{
  instance = this;

  //IDIOMA
    Locale::setLang(ConfigurationController::getConfig("lang"));
    RF_Engine::Debug("Idioma configurado: " + Locale::getLang());

  //CONFIGURAMOS LOS EJES
    InputManager::setAxis("exeControl",
        KeyGroup::Make({&RF_Input::key[_esc], &RF_Input::key[_close_window], &RF_Input::jkey[_select]}),
        KeyGroup::Make({})
    );
    InputManager::setAxis("Horizontal",
        KeyGroup::Make({&RF_Input::key[_d], &RF_Input::key[_right], &RF_Input::jkey[_jpad_dir_right]}),
        KeyGroup::Make({&RF_Input::key[_a], &RF_Input::key[_left], &RF_Input::jkey[_jpad_dir_left]})
    );
    InputManager::setAxis("Vertical",
        KeyGroup::Make({&RF_Input::key[_s], &RF_Input::key[_down], &RF_Input::jkey[_jpad_dir_down]}),
        KeyGroup::Make({&RF_Input::key[_w], &RF_Input::key[_up], &RF_Input::jkey[_jpad_dir_up]})
    );
    InputManager::setAxis("Selection",
        KeyGroup::Make({&RF_Input::key[_return], &RF_Input::key[_space], &RF_Input::jkey[_circle]}),
        KeyGroup::Make({})
    );

  //CREAMOS LA VENTANA
    fscrn = (ConfigurationController::getConfig("fullscreen") == "true");
    window = RF_Engine::addWindow("Begi is you", 1280, 720, SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, (!fscrn) ? SDL_WINDOW_OPENGL : SDL_WINDOW_OPENGL|SDL_WINDOW_FULLSCREEN);
    RF_Engine::MainWindow(window);

  //ASIGNAMOS EL COLOR A LA VENTANA
    window->setBackColor(0x00, 0x00, 0x00);

  //CARGAMOS EL CONTROLADOR DE RECURSOS
    ResourceController::LoadResources();

  //CREAMOS EL CONTROLADOR DE JUEGO
     RF_Engine::newTask<GameController>(id);

  //CARGAMOS LAS ESCENAS
    SceneManager::RegisterScene<Splash>();
    SceneManager::RegisterScene<Mainmenu>();


  //INICIAMOS LA PRIMERA ESCENA
    //SceneManager::Start(startingScene);
    GameController::instance->LoadLevel("level1", id);
}

void MainProcess::Update()
{
  /*if(InputManager::getAxis("exeControl") != 0 )
  {
    if(!ConfigurationController::EscPressed())
    {
      RF_Engine::Status() = false;
    }

    ConfigurationController::EscPressed() = true;
  }
  else
  {
      ConfigurationController::EscPressed() = false;
  }*/
}

void MainProcess::NavigateTo(string scene, bool top)
{
  if(top) SceneManager::Start(scene);
  else    SceneManager::Change(scene);
}

void MainProcess::NavigateBack()
{
  SceneManager::Stop();
}
